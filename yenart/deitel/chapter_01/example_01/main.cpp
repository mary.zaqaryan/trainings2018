/// This program greats the world
#include <iostream> /// input/output system

/// the starting point of the program
int
main()
{
    /// prints the message
    std::cout << "Hello World!\n";

    /// exit status is 0
    return 0;
} /// end of main

