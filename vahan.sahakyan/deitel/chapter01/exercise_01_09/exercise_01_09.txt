1.9 Give a brief answer to each of the following questions:
a. Why does this text discuss structured programming in addition to object-oriented programming?
b. What are the typical steps (mentioned in the text) of an object-oriented design process?
c. What kinds of messages do people send to one another?
d. Objects send messages to one another across well-defined interfaces. What interfaces does a car radio (object) present to its user (a
person object)?

Answers:
a.It show the advantages and disadvantages of both.
b.	1.ANALIZE project's requirements
	2.Develope a DESIGN
	3.IMPLEMENT (or execute)
c.Short, straight, easily understandable, not complex.
d.Graphical User Interfaces (GUIs) (example: touchscreen)
